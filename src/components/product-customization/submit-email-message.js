import React from 'react';

const SubmitEmailMessage = () => {
  return (
    <div id="submit-email-message">
      <button className="close-btn">X</button>
      <div id="email-message">
        <p>Thank you for submitting your email for logo customization. Our staff will email
        you in the next twenty four hours the logo specifications that you need to submit.</p>
        <button>Submit</button>
      </div>
    </div>
  );
};

export default SubmitEmailMessage;
