import React from 'react';

const ProductCustomizationOneNote = () => {
  return (
    <div id="product-customization-one-note">
      <a href="#"></a>
      <span id="gift-note-icon"></span>
      <span id="gift-note-message">Please, write your message and hit save when finished.</span>
      <form>
        <textarea defaultValue="Enter note here"/>
        <button className="brown-color" id="note-save-btn">Save</button>
      </form>
      <button className="brown-color">Continue Shopping</button>
      <button>Add to cart</button>
    </div>
  );
};

export default ProductCustomizationOneNote;
