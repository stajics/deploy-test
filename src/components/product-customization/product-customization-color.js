import React from 'react';

const ProductCustomizationColor = () => {
  return (
    <div id="product-customization-color" className="clearfix">
      <ol className="product-customization-status" data-progtrckr-steps="3">
        <li><a href="#" id="product-customization-back-btn"></a></li>
        <li className="product-customization-status-done"></li>
        <li className="product-customization-status-todo" id="step-2"></li>
        <li className="product-customization-status-todo" id="step-3"></li>
        <li className="product-customization-status-todo" id="step-4"></li>
        <li className="product-customization-status-todo" id="step-5"></li>
      </ol>
      <h2>Select Color</h2>
      <span className="select-box-dimensions-lbl">Box Dimensions 25 x 25 x 10 cm, Weight 500 g ~ 30 pcs</span>
      <div className="select-box-type-1">
        <div>
          <a className="magnifier-btn" href="#"></a>
          <span className="select-box-type-1-price">$35</span>
        </div>
        <div className="select-box-type-1-image"></div>
        <a href="#" className="select-box-type-1-next-btn">Silver</a>
        <a href="#" className="product-customization-next-btn"></a>
      </div>
      <div className="select-box-type-2">
        <div>
          <a className="magnifier-btn" href="#"></a>
          <span className="select-box-type-2-price">$35</span>
        </div>
        <div>
        <div className="select-box-type-2-image"></div>
        <a href="#" className="select-box-type-2-next-btn">Gold</a>
        <a href="#" className="product-customization-next-btn"></a>
        </div>
      </div>
    </div>
  );
};

export default ProductCustomizationColor;
