import React from 'react';

const ProductCustomizationSelectNote = () => {
  return(
    <div id="product-customization-select-note">
      <ol className="product-customization-status" data-progtrckr-steps="3">
        <li><a href="#"></a></li>
        <li className="product-customization-status-done"></li>
        <li className="product-customization-status-todo" id="step-2"></li>
        <li className="product-customization-status-todo" id="step-3"></li>
        <li className="product-customization-status-todo" id="step-4"></li>
        <li className="product-customization-status-todo" id="step-5"></li>
      </ol>
    </div>
  );
};

export default ProductCustomizationSelectNote;
