import React from 'react';

const ProductCustomizationBoxType = () => {
  return(
    <div id="product-customization-box-type">
      <h2>Select Box Type</h2>
      <div id="square-box">
        <span>Square Base Box</span>
        <span className="floated-right">$35</span>
        <div id="square-box-image"></div>
        <button>Box dimensions - 25 x 25 x 10 cm Weight 500 g ~ 30 pieces</button>
      </div>
      <div id="rectangular-box">
        <span>Rectangle Base Box</span>
        <span className="floated-right">$30</span>
        <div></div>
        <button>Box dimensions - 30 x 20 x 8 cm Weight 500 g ~ 24 pieces</button>
      </div>
    </div>
  );
};

export default ProductCustomizationBoxType;
