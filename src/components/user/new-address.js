import React from 'react';

const NewAddress = () => {
  return (
    <div id="new-address" className="clearfix">
      <form>
        <span id="add-an-address-title">Add a New Address</span>
        <div id="address-first-column">
          <div>
            <label>First Name</label>
            <input type="text" />
          </div>
          <div>
            <label>Last Name</label>
            <input type="text" />
          </div>
          <div>
            <select>
              <option>Select Country</option>
            </select>
          </div>
          <div>
            <select>
              <option>Select City</option>
            </select>
          </div>
          <div>
            <select>
              <option>Select Area</option>
            </select>
          </div>
          <div>
            <label>Street</label>
            <input type="text" />
          </div>
          <div>
            <label>Building</label>
            <input type="text" />
          </div>
        </div>
        <div id="address-second-column">
          <div>
            <label>Floor (optional)</label>
            <input type="text" />
          </div>
          <div>
            <label>Appartment (optional)</label>
            <input type="text" />
          </div>
          <div>
            <label>Nearest Landmark (optional)</label>
            <input type="text" />
          </div>
          <div>
            <select>
              <option>Location Type</option>
            </select>
          </div>
          <div>
            <select>
              <option>Preferred Delivery Time</option>
            </select>
          </div>
          <div>
            <label>+971 Mobile Number</label>
            <input type="text" />
          </div>
          <div>
            <label>+971 Landline Number</label>
            <input type="text" />
          </div>
        </div>
        <div id="address-third-column">
          <textarea defaultValue="Shipping Notes" />
          <div>
            <button id="new-address-cancel-btn">Cancel</button>
            <button id="new-address-add-btn">Add Address</button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default NewAddress;
