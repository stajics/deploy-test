import React from 'react';

const Registration = () => {
  return (
    <div id="registration">
      <form>
      <div id="firstname">
        <label>First Name</label>
        <input className="registration-name" type="text" />
      </div>
      <div id="lastname">
        <label>Last Name</label>
        <input className="registration-name" type="text" />
      </div>
      <div id="email">
        <label>Email</label>
        <input type="text" />
      </div>
      <div className="password" id="password-content">
        <label>Password</label>
        <input type="password" />
      </div>
      <div className="password" id="show-password">
        <label id="show-password-lbl">Show Password</label>
        <input type="checkbox" id="show-password"/>
      </div>
      <div>
        <select className="gender">
          <option>Male</option>
          <option>Female</option>
        </select>
        <select className="country">
          <option>UAE</option>
          <option>KSA</option>
          <option>USA</option>
        </select>
      </div>
      <div id="registration-subscription" className="clearfix">
        <input type="checkbox" className="inline-block"/>
        <label id="subscription-lbl" className="inline-block">
        I would like to register to Fodakty deals and save up to 70%
        </label>
      </div>
      <div id="sign-up">
        <button>SIGN UP</button>
        <label>By clicking the SIGN UP button you confirm that you accept our <a>Terms of Use</a> and <a>Privacy Policy</a></label>
      </div>
      </form>
    </div>
  );
};

export default Registration;
