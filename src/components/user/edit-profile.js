import React from 'react';

const EditProfile = () => {
  return (
    <div id="edit-profile">
      <form>
        <div>
          <label>First Name</label>
          <input type="text" />
        </div>
        <div>
          <label>Last Name</label>
          <input type="text" />
        </div>
        <div>
          <label>Username</label>
          <input type="text" />
        </div>
        <div>
          <label>Email</label>
          <input type="email" />
        </div>
        <div>
          <button>Update Profile</button>
          <a href="#" id="reset-password-btn">Reset Password</a>
        </div>
      </form>
    </div>
  );
};

export default EditProfile;
