import React from 'react';

const Addresses = () => {
  return (
    <div id="addresses">
      <div className="address clearfix">
        <div>
          <a className="address-edit" href="#"></a>
          <div className="address-name">Mohammed Alzubi</div>
          <div>Al sa'ada st., Business Bay, Dubai</div>
        </div>
        <div className="address-country-section">
          <a className="address-delete" href="#"></a>
          <div className="address-country">UAE</div>
          <div>95461248762</div>
        </div>
      </div>
      <div className="address clearfix">
        <div>
          <a className="address-edit" href="#"></a>
          <div className="address-name">Mohammed Alzubi</div>
          <div>Al sa'ada st., Business Bay, Dubai</div>
        </div>
        <div className="address-country-section">
          <a className="address-delete" href="#"></a>
          <div className="address-country">UAE</div>
          <div>95461248762</div>
        </div>
      </div>
      <button>Add a New Address</button>
    </div>
  );
};

export default Addresses;
