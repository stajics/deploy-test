import React from 'react';

const Login = () => {
  return (
    <div id="login">
      <form>
        <label id="email-lbl">Email</label>
        <input type="text" />
        <label id="password-lbl">Password</label>
        <input type="password"/>
        <button id="login-btn">Login</button>
        <a href="#" id="forgot-password" className="clearfix">Forgot Password?</a>
        <label id="social-logins-label">Or login with...</label>
        <div id="social-logins">
          <button></button>
        </div>
        <div id="sign-up">
          <label>Don't have an account yet?</label>
          <a href="#">SIGN UP</a>
        </div>
      </form>
    </div>
  );
};

export default Login;
