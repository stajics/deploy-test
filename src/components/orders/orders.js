import React from 'react';

const Orders = () => {
  return (
    <div id="orders">
      <div className="order clearfix">
        <div className="order-image clearfix"></div>
        <div className="order-details clearfix">
          <span className="order-name">Brown Ribbon Box</span>
          <span className="order-price">50 $</span>
          <div className="order-number">Order Number: 1458765</div>
          <div className="order-date">Order Date: 9 Jan 2016</div>
        </div>
        <div className="order-shipment-status-container">
          <label>Shipment Status: Not Yet Shipped</label>
          <ol className="order-shipment-status" data-progtrckr-steps="3">
            <li className="order-shipment-status-done"></li>
            <li className="order-shipment-status-todo"></li>
            <li className="order-shipment-status-todo"></li>
          </ol>
          <div className="order-shipment-status-labels-container">
            <span>In Processing</span>
            <span>Shipped</span>
            <span>Delivered</span>
          </div>
        </div>
        <button>Track Shipment</button>
      </div>
      <div className="order clearfix">
        <div className="order-image clearfix"></div>
        <div className="order-details clearfix">
          <span className="order-name">Brown Ribbon Box</span>
          <span className="order-price">50 $</span>
          <div className="order-number">Order Number: 1458765</div>
          <div className="order-date">Order Date: 9 Jan 2016</div>
        </div>
        <div className="order-shipment-status-container">
          <label>Shipment Status: Not Yet Shipped</label>
          <ol className="order-shipment-status" data-progtrckr-steps="3">
            <li className="order-shipment-status-done"></li>
            <li className="order-shipment-status-todo"></li>
            <li className="order-shipment-status-todo"></li>
          </ol>
          <div className="order-shipment-status-labels-container">
            <span>In Processing</span>
            <span>Shipped</span>
            <span>Delivered</span>
          </div>
        </div>
        <button>Track Shipment</button>
      </div>
    </div>
  );
};

export default Orders;
