import React from 'react';

const ReturnOrder = () => {
  return (
    <div id="return-an-order">
      <div id="return-order-details">
        <span>Order Number: 1458765</span>
        <span>Order Date: 9 Jan 2017</span>
        <span>Shipment: 1 of 1</span>
      </div>
      <div id="items-to-return" className="clearfix">
        <div className="return-an-order-checkbox-container">
          <input type="checkbox" />
        </div>
        <div className="cart-item clearfix">
          <div className="cart-item-image clearfix"></div>
          <div className="cart-item-details clearfix">
            <span className="cart-item-name">Brown Ribbon Box</span>
            <span className="cart-item-price">50 $</span>
            <p className="cart-item-description">Golden brown with smooth,
            semi-translucent skin and flesh that is delicate and toffee flavoured
            but not too sweet.</p>
          </div>
        </div>
      </div>
      <div id="complete-return-container">
        <button>Complete Return</button>
      </div>
    </div>
  );
};

export default ReturnOrder;
