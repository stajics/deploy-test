import React from 'react';

const SuccessfulOrder = () => {
  return (
    <div id="successful-order">
      <h2>Your order was placed successfully</h2>
      <div id="successful-order-details">
        <span>Order Number: 1458765</span>
        <span>Order Date: 9 Jan 2017</span>
        <span>Order Price: $50</span>
      </div>
      <div id="successful-order-items" className="clearfix">
        <div className="cart-item clearfix">
          <div className="cart-item-image clearfix"></div>
          <div className="cart-item-details clearfix">
            <span className="cart-item-name">Brown Ribbon Box</span>
            <span className="cart-item-price">50 $</span>
            <p className="cart-item-description">Golden brown with smooth,
            semi-translucent skin and flesh that is delicate and toffee flavoured
            but not too sweet.</p>
          </div>
        </div>
        <div className="cart-item clearfix">
          <div className="cart-item-image clearfix"></div>
          <div className="cart-item-details clearfix">
            <span className="cart-item-name">Brown Ribbon Box</span>
            <span className="cart-item-price">50 $</span>
            <p className="cart-item-description">Golden brown with smooth,
            semi-translucent skin and flesh that is delicate and toffee flavoured
            but not too sweet.</p>
          </div>
        </div>
      </div>
      <div id="people-also-bought" className="clearfix">
        <h2 id="people-also-bought-heading">People also bought</h2>
        <div className="search-trending-search">
          <div className="trending-search-product-image"></div>
          <label>Silver Ribbon Box</label>
          <div id="search-buttons">
            <a id="wishlist-btn" href="#"></a>
            <a id="add-to-cart-btn" href="#"></a>
            <a id="share-btn" href="#"></a>
          </div>
        </div>
        <div className="search-trending-search">
          <div className="trending-search-product-image"></div>
          <label>Silver Ribbon Box</label>
          <div id="search-buttons">
            <a id="wishlist-btn" href="#"></a>
            <a id="add-to-cart-btn" href="#"></a>
            <a id="share-btn" href="#"></a>
          </div>
        </div>
      </div>
      <div id="return-to-home-page">
        <button>Return to home page</button>
      </div>
    </div>
  );
};

export default SuccessfulOrder;
