import React from 'react';

const Cart = () => {
  return (
    <div id="cart">
      <div className="cart-item clearfix">
        <div className="cart-item-image clearfix"></div>
        <div className="cart-item-details clearfix">
          <span className="cart-item-name">Brown Ribbon Box</span>
          <span className="cart-item-price">50 $</span>
          <p className="cart-item-description">Golden brown with smooth,
          semi-translucent skin and flesh that is delicate and toffee flavoured
          but not too sweet.</p>
          <div className="cart-item-actions">
            <a href="#">
              <span className="delete-btn-icon"></span>
              <span className="delete-btn-text">Delete</span>
            </a>
            <label className="cart-quantity-lbl">Quantity</label>
          </div>
          <div className="cart-item-actions">
            <div className="move-to-wishlist">
              <a href="#">
                <span className="wishlist-btn-icon"></span>
                <span className="wishlist-btn-text">Move to wishlist</span>
              </a>
            </div>
            <div>
              <input type="number" defaultValue="0" min="0"/>
            </div>
          </div>
        </div>
      </div>
      <div className="cart-item clearfix">
        <div className="cart-item-image clearfix"></div>
        <div className="cart-item-details clearfix">
          <span className="cart-item-name">Brown Ribbon Box</span>
          <span className="cart-item-price">50 $</span>
          <p className="cart-item-description">Golden brown with smooth,
          semi-translucent skin and flesh that is delicate and toffee flavoured
          but not too sweet.</p>
          <div className="cart-item-actions">
            <a href="#">
              <span className="delete-btn-icon"></span>
              <span className="delete-btn-text">Delete</span>
            </a>
            <label className="cart-quantity-lbl">Quantity</label>
          </div>
          <div className="cart-item-actions">
            <div className="move-to-wishlist">
              <a href="#">
                <span className="wishlist-btn-icon"></span>
                <span className="wishlist-btn-text">Move to wishlist</span>
              </a>
            </div>
            <div>
              <input type="number" defaultValue="0" min="0"/>
            </div>
          </div>
        </div>
      </div>
      <div className="cart-item clearfix">
        <div className="cart-item-image clearfix"></div>
        <div className="cart-item-details clearfix">
          <span className="cart-item-name">Brown Ribbon Box</span>
          <span className="cart-item-price">50 $</span>
          <p className="cart-item-description">Golden brown with smooth,
          semi-translucent skin and flesh that is delicate and toffee flavoured
          but not too sweet.</p>
          <div className="cart-item-actions">
            <a href="#">
              <span className="delete-btn-icon"></span>
              <span className="delete-btn-text">Delete</span>
            </a>
            <label className="cart-quantity-lbl">Quantity</label>
          </div>
          <div className="cart-item-actions">
            <div className="move-to-wishlist">
              <a href="#">
                <span className="wishlist-btn-icon"></span>
                <span className="wishlist-btn-text">Move to wishlist</span>
              </a>
            </div>
            <div>
              <input type="number" defaultValue="0" min="0"/>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Cart;
