import React from 'react';

const Checkout = () => {
  return (
    <div id="checkout" className="clearfix">
      <div id="delivery-options">
        <div id="way-of-delivery">
          <span id="delivery-options-lbl">Delivery Options</span>
          <input type="checkbox" />
          <label>Ship to my delivery address</label>
          <span id="develiry-options-details-lbl">Ships within 2 business days for $12.</span>
        </div>
        <div id="delivery-options-personal-details">
          <span id="customer-name" className="bold">Mohammed Alzubi</span>
          <span>Building A, Al saada st., Business Bay, Dubai</span>
          <span className="bold">UAE</span>
          <span>95461248762</span>
        </div>
        <button>Change Address</button>
      </div>
      <div id="shipment-details">
        <span id="shipment-details-lbl">Shipment details</span>
        <div className="order clearfix">
          <div className="order-image clearfix"></div>
          <div className="order-details clearfix">
            <span className="order-name">Brown Ribbon Box</span>
            <span className="order-price">50 $</span>
            <div className="order-number">Order Number: 1458765</div>
            <div className="order-date">Order Date: 9 Jan 2016</div>
          </div>
        </div>
        <div id="shipping-delivery-options">
          <input type="checkbox" />
          <label>Standard Domestic Shipping Service</label>
          <span>Ships within 2 business days for $12</span>
          <span>By Express Uno</span>
          <span>Quantity: 1</span>
        </div>
      </div>
      <div id="payment-summary">
        <span id="payment-summary-lbl">Payment Summary</span>
        <div>
          <span className="floated-left">Total</span>
          <span className="floated-right">$50</span>
        </div>
        <div>
          <span className="floated-left">Total Shipment</span>
          <span className="floated-right">$12</span>
        </div>
        <div>
          <span className="floated-left">Grand Total</span>
          <span className="floated-right">$62</span>
        </div>
        <div>
          <span className="floated-left" id="got-coupon-lbl">Got a coupon?</span>
          <a href="#">Enter here</a>
        </div>
        <button>Place order</button>
      </div>
    </div>
  );
};

export default Checkout;
