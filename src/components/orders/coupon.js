import React from 'react';

const Coupon = () => {
  return(
    <div id="coupon">
    <button className="close-btn">X</button>
      <div id="coupon-area">
        <label>Enter Coupon Code</label>
        <input type="text" />
        <button>Apply</button>
      </div>
    </div>
  );
};

export default Coupon
