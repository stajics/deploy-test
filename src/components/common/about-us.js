import React from 'react';

const AboutUS = () => {
  return (
    <div id="about-us">
      <h2>Lorem ipsum dolor sit amet</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc congue eleifend dui id pulvinar.
      Praesent fermentum efficitur ante, at suscipit metus placerat efficitur. Vivamus gravida fermentum elit sed molestie.
      In faucibus vulputate ligula quis pellentesque. Curabitur a lorem pretium, viverra dui a, luctus velit.
      Sed porttitor dolor vestibulum, rhoncus est sed, dapibus nibh.
      Maecenas sit amet metus eget nisi molestie varius nec ultricies nulla.
      Nam tempus dui sit amet suscipit venenatis. Nulla ornare, augue vitae vestibulum consequat,
      ex nunc egestas quam, vitae vestibulum libero odio ac est. Nam eu urna nisi.
      Fusce condimentum eros ante, eu dignissim enim placerat sit amet. Aenean sapien sem,
      auctor at vestibulum sed, efficitur in nisl. Integer vel rhoncus arcu, in dictum nunc.</p>

      <p>Cras quis nunc in odio rhoncus fermentum at feugiat ligula. Maecenas tortor justo,
      aliquet eget ipsum ac, ultrices bibendum odio. Fusce at cursus arcu. Duis id euismod sapien,
      et dignissim felis. Aenean purus lacus, lacinia et lacinia a, cursus at nibh.
      Duis feugiat sit amet justo nec porta. Ut non maximus arcu. Curabitur mattis ante vitae turpis suscipit,
      vel dictum tortor imperdiet. Nulla commodo vitae est eu hendrerit. Donec finibus,
      ipsum sit amet dapibus condimentum, est urna malesuada arcu, vitae vehicula nunc lorem iaculis mauris.
      Nunc malesuada commodo velit. Sed eget lectus a sapien eleifend molestie vitae sed nulla.
      Nunc eget dictum quam, nec condimentum tellus. Etiam vitae nisl congue, egestas sem a, tristique arcu.
      Sed maximus justo id lorem congue mollis.
      </p>
    </div>
  );
};

export default AboutUS;
