import React from 'react';

const Home = () => {
  return (
    <div id="home">
      <div className="slider-holder">
            <span id="slider-image-1"></span>
            <span id="slider-image-2"></span>
            <div className="image-holder">
                <img src="../resources/square-box-home-page.png" className="slider-image" />
                <img src="../resources/rectangular-box-home-page.png"/>
            </div>
            <div className="button-holder">
                <a href="#slider-image-1" className="slider-change"></a>
                <a href="#slider-image-2" className="slider-change"></a>
            </div>
        </div>
    </div>
  );
};

export default Home;
