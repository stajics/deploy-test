import React from 'react';

const Inquiry = () => {
  return (
    <div id="inquiry">
      <form>
        <div id="inquiry-email">
          <label>Your email address*</label>
          <input type="text" />
        </div>
        <div id="inquiry-help-option">
          <label>How can we help?*</label>
          <select>
            <option></option>
          </select>
        </div>
        <div id="inquiry-message">
          <label>Message*</label>
          <textarea />
        </div>
        <div id="inquiry-phone">
          <label>Your mobile number*</label>
          <input type="text" />
        </div>
        <button>Send Request</button>
        </form>
      </div>
  );
};

export default Inquiry;
