import React from 'react';

const ContactUS = () => {
  return (
    <div id="contact-us">
      <label className="centered">Our goal is 100% satisfaction</label>
      <div id="contact-us-buttons">
        <button>Track your Order</button>
        <button>Return an Order</button>
        <button>Cancel an Order</button>
        <button>Other Inquiry</button>
      </div>
      <div id="customer-service">
        <label className="centered">Customer Service</label>
        <div id="floated-left"><a id="call" href="#">Call Us (+971)-4-437 0900</a></div>
        <div id="floated-right" className="clearfix"><a id="contact" href="#">Contact Us</a></div>
        <div className="clear"></div>
      </div>
      <div id="contact-us-social">
        <label>Follow Us</label>
        <button id="follow-facebook" className="bordered-right"></button>
        <button id="follow-twitter" className="bordered-right"></button>
        <button id="follow-instagram"></button>
      </div>
      <div id="contact-us-stores">
        <label>Download our Apps</label>
        <button id="app-store"></button>
        <button id="google-play"></button>
      </div>
    </div>
  );
};

export default ContactUS;
