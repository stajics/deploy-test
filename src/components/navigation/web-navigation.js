import React from 'react';

const WebNavigation = () => {
  return (
    <nav>
      <ul id="web-nav">
        <li>
          <a id="logo-nav" href="#"></a>
        </li>
        <li >
          <span id="home-nav-icon"></span>
          <a href="#">Home</a>
        </li>
        <li id="store-nav">
          <span id="store-nav-icon"></span>
          <a href="#">Store</a>
          <div id="store-nav-content">
            <div id="store-date-types">
              <div className="store-nav-element-lbl">Date Types</div>
              <div className="date-types">
                <span>Ajvah</span><span>Anbarah</span><span>Baydh</span>
              </div>
              <div className="date-types">
                <span>Ajvah</span><span>Anbarah</span><span>Baydh</span>
              </div>
              <div className="date-types">
                <span>Ajvah</span><span>Anbarah</span><span>Baydh</span>
              </div>
              <div className="date-types">
                <span>Ajvah</span><span>Anbarah</span><span>Baydh</span>
              </div>
            </div>
            <div id="store-special-occasions">
              <div className="store-nav-element-lbl">Special Occasions</div>
              <div className="special-occasion">
                <span className="store-special-occasion-image"></span>
                <span className="store-special-occasion-name">Christmas</span>
              </div>
              <div className="special-occasion">
                <span className="store-special-occasion-image"></span>
                <span className="store-special-occasion-name">Ramadan</span>
              </div>
              <div className="special-occasion">
                <span className="store-special-occasion-image"></span>
                <span className="store-special-occasion-name">Easter</span>
              </div>
            </div>
          </div>
        </li>
        <li>
          <span id="gift-nav-icon"></span>
          <a href="#">Gift</a>
        </li>
        <li>
          <span id="deals-nav-icon"></span>
          <a href="#">Deals</a>
        </li>
        <li>
          <span id="news-nav-icon"></span>
          <a href="#">News</a>
        </li>
        <li>
          <span id="about-us-nav-icon"></span>
          <a href="#">About us</a>
        </li>
        <li>
          <span id="contact-us-nav-icon"></span>
          <a href="#">Contacts</a>
        </li>
        <li id="profile-nav">
          <a href="#" id="profile-nav-icon"></a>
          <div id="profile-nav-content">
            <div id="profile-personal-settings">
              <div id="profile-settings-buttons">
                <a href="#" id="profile-settings">Settings</a>
                <a href="#" id="profile-logout">Logout</a>
              </div>
              <div id="profile-image"></div>
              <span id="profile-name">Mohammed Alzubi</span>
              <span id="profile-email">mohammedalzubi@gmail.com</span>
            </div>
            <div id="profile-orders-buttons">
              <a href="#" id="my-wishlist">My Wishlist</a>
              <a href="#" id="my-orders">My Orders</a>
              <a href="#" id="my-addresses">My Addresses</a>
            </div>
            <div id="profile-country-language">
              <span id="country-and-language-lbl">Country and Language</span>
              <div id="countries-languages-container">
                <a href="#" id="country-UAE">UAE</a>
                <a href="#" id="country-KSA">KSA</a>
                <a href="#" id="country-USA">USA</a>
                <a href="#" id="lang-arabe">Arabe</a>
                <a href="#" id="lang-english">English</a>
              </div>
            </div>
          </div>
        </li>
        <li id="cart-nav">
          <a href="#" id="cart-nav-icon"></a>
          <div id="cart-nav-content">
            <div className="cart-nav-item">
              <div className="cart-nav-item-image"></div>
              <div className="cart-nav-item-details">
                <span className="cart-nav-item-name">Silver Ribbon Box</span>
                <span className="cart-nav-item-price">$65</span>
                <input type="number" />
              </div>
            </div>
            <div className="cart-nav-item clearfix">
              <div className="cart-nav-item-image"></div>
              <div className="cart-nav-item-details">
                <span className="cart-nav-item-name">Silver Ribbon Box</span>
                <span className="cart-nav-item-price">$65</span>
                <input type="number" />
              </div>
            </div>
            <div className="cart-nav-details clearfix">
              <span id="subtotal">Sub Total</span>
              <span id="subtotal-price">$180</span>
              <button>View Cart</button>
              <button>Checkout</button>
              <span id="apply-coupon">Apply coupon in next step.</span>
            </div>
          </div>
        </li>
        <li>
          <a href="#" id="search-nav-icon"></a>
        </li>
      </ul>
    </nav>
  );
};

export default WebNavigation;
