import React from 'react';

const SearchResult = () => {
  return (
    <div id="search" className="clearfix">
      <input type="text" defaultValue="Search..." id="search-field"/>
      <div id="search-filter">
        <span id="filter">Filter</span>
        <div id="search-filter-price">
          <label>Price</label>
          <input type="range" />
        </div>
        <select id="weight">
        </select>
        <div className="search-filter-checkbox">
          <input type="checkbox" />
          <label>Custom Ribbon</label>
        </div>
        <select id="search-filter-dates-type">
          <option>Dates Type</option>
        </select>
        <select id="search-filter-color">
        </select>
        <div className="search-filter-checkbox">
          <input type="checkbox" />
          <label>Custom Note</label>
        </div>
        <select id="search-filter-brand"></select>
        <div id="submit-buttons">
          <input type="submit" defaultValue="apply" />
          <input type="reset" />
        </div>
      </div>
      <div id="search-trending-searches">
        <span id="search-result-lbl">Search Results</span>
        <div className="product clearfix">
          <span className="product-discount-label">20% Off</span>
          <a className="product-wishlist-btn" href="#"></a>
          <div className="product-image"></div>
          <h2>Fruit Palm - Gift Ramadan</h2>
          <p>Golden brown with smooth, semi-translucent skin and flesh that is delicate
          and toffee flavoured, but not too sweet.</p>
          <span className="product-price">$70</span>
          <span className="product-discounted-price">$50</span>
          <button className="product-add-to-cart-btn">Add to Cart</button>
        </div>
        <div className="product clearfix">
          <span className="product-discount-label">20% Off</span>
          <a className="product-wishlist-btn" href="#"></a>
          <div className="product-image"></div>
          <h2>Fruit Palm - Gift Ramadan</h2>
          <p>Golden brown with smooth, semi-translucent skin and flesh that is delicate
          and toffee flavoured, but not too sweet.</p>
          <span className="product-price">$70</span>
          <span className="product-discounted-price">$50</span>
          <button className="product-add-to-cart-btn">Add to Cart</button>
        </div>
        <div className="product clearfix">
          <span className="product-discount-label">20% Off</span>
          <a className="product-wishlist-btn" href="#"></a>
          <div className="product-image"></div>
          <h2>Fruit Palm - Gift Ramadan</h2>
          <p>Golden brown with smooth, semi-translucent skin and flesh that is delicate
          and toffee flavoured, but not too sweet.</p>
          <span className="product-price">$70</span>
          <span className="product-discounted-price">$50</span>
          <button className="product-add-to-cart-btn">Add to Cart</button>
        </div>
        <div className="product clearfix">
          <span className="product-discount-label">20% Off</span>
          <a className="product-wishlist-btn" href="#"></a>
          <div className="product-image"></div>
          <h2>Fruit Palm - Gift Ramadan</h2>
          <p>Golden brown with smooth, semi-translucent skin and flesh that is delicate
          and toffee flavoured, but not too sweet.</p>
          <span className="product-price">$70</span>
          <span className="product-discounted-price">$50</span>
          <button className="product-add-to-cart-btn">Add to Cart</button>
        </div>
      </div>
    </div>
  );
};

export default SearchResult;
