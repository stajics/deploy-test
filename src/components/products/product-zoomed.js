import React from 'react';

const ProductZoomed = () => {
  return (
      <div id="product-zoomed">
        <button className="close-btn">X</button>
        <div id="product-zoomed-image"></div>
      </div>
  );
};

export default ProductZoomed;
