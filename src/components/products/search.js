import React from 'react';

const Search = () => {
  return (
    <div id="search" className="clearfix">
      <input type="text" defaultValue="Search..." id="search-field" />
      <div id="search-filter">
        <span id="filter">Filter</span>
        <div id="search-filter-price">
          <label>Price</label>
          <input type="range" />
        </div>
        <select id="weight">
          <option>Weight</option>
        </select>
        <div className="search-filter-checkbox">
          <input type="checkbox" />
          <label>Custom Ribbon</label>
        </div>
        <select id="search-filter-dates-type">
          <option>Dates Type</option>
        </select>
        <select id="search-filter-color">
          <option>Color</option>
        </select>
        <div className="search-filter-checkbox">
          <input type="checkbox" />
          <label>Custom Note</label>
        </div>
        <div>
          <select id="search-filter-brand">
            <option>Brand</option>
          </select>
        </div>
        <div id="submit-buttons">
          <input type="submit" defaultValue="apply" />
          <input type="reset" />
        </div>
      </div>
      <div id="search-trending-searches">
        <span id="trending-searches-lbl">Trending Searches</span>
        <div className="search-trending-search right-margin">
          <div className="trending-search-product-image"></div>
          <label>Silver Ribbon Box</label>
          <div id="search-buttons">
            <a id="wishlist-btn"></a>
            <a id="add-to-cart-btn"></a>
            <a id="share-btn"></a>
          </div>
        </div>
        <div className="search-trending-search right-margin">
          <div className="trending-search-product-image"></div>
          <label>Silver Ribbon Box</label>
          <div id="search-buttons">
            <a id="wishlist-btn"></a>
            <a id="add-to-cart-btn"></a>
            <a id="share-btn"></a>
          </div>
        </div>
        <div className="search-trending-search">
          <div className="trending-search-product-image"></div>
          <label>Silver Ribbon Box</label>
          <div id="search-buttons">
            <a id="wishlist-btn"></a>
            <a id="add-to-cart-btn"></a>
            <a id="share-btn"></a>
          </div>
        </div>
        <div className="search-trending-search right-margin">
          <div className="trending-search-product-image"></div>
          <label>Silver Ribbon Box</label>
          <div id="search-buttons">
            <a id="wishlist-btn" href="#"></a>
            <a id="add-to-cart-btn" href="#"></a>
            <a id="share-btn" href="#"></a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Search;
