import React from 'react';

const Wishlist = () => {
  return (
    <div id="wishlist" className="clearfix">
      <div className="product clearfix">
        <span className="product-discount-label">20% Off</span>
        <a className="product-remove-btn" href="#"></a>
        <div className="product-image"></div>
        <h2>Fruit Palm - Gift Ramadan</h2>
        <p>Golden brown with smooth, semi-translucent skin and flesh that is delicate
        and toffee flavoured, but not too sweet.</p>
        <span className="product-price">$70</span>
        <span className="product-discounted-price">$50</span>
        <button className="product-add-to-cart-btn">Add to Cart</button>
      </div>
      <div className="product clearfix">
        <span className="product-discount-label">20% Off</span>
        <a className="product-remove-btn" href="#"></a>
        <div className="product-image"></div>
        <h2>Fruit Palm - Gift Ramadan</h2>
        <p>Golden brown with smooth, semi-translucent skin and flesh that is delicate
        and toffee flavoured, but not too sweet.</p>
        <span className="product-price">$70</span>
        <span className="product-discounted-price">$50</span>
        <button className="product-add-to-cart-btn">Add to Cart</button>
      </div>
      <div className="product clearfix">
        <span className="product-discount-label">20% Off</span>
        <a className="product-remove-btn" href="#"></a>
        <div className="product-image"></div>
        <h2>Fruit Palm - Gift Ramadan</h2>
        <p>Golden brown with smooth, semi-translucent skin and flesh that is delicate
        and toffee flavoured, but not too sweet.</p>
        <span className="product-price">$70</span>
        <span className="product-discounted-price">$50</span>
        <button className="product-add-to-cart-btn">Add to Cart</button>
      </div>
      <div className="product clearfix">
        <span className="product-discount-label">20% Off</span>
        <a className="product-remove-btn" href="#"></a>
        <div className="product-image"></div>
        <h2>Fruit Palm - Gift Ramadan</h2>
        <p>Golden brown with smooth, semi-translucent skin and flesh that is delicate
        and toffee flavoured, but not too sweet.</p>
        <span className="product-price">$70</span>
        <span className="product-discounted-price">$50</span>
        <button className="product-add-to-cart-btn">Add to Cart</button>
      </div>
      <div className="product clearfix">
        <span className="product-discount-label">20% Off</span>
        <a className="product-remove-btn" href="#"></a>
        <div className="product-image"></div>
        <h2>Fruit Palm - Gift Ramadan</h2>
        <p>Golden brown with smooth, semi-translucent skin and flesh that is delicate
        and toffee flavoured, but not too sweet.</p>
        <span className="product-price">$70</span>
        <span className="product-discounted-price">$50</span>
        <button className="product-add-to-cart-btn">Add to Cart</button>
      </div>
      <div className="product clearfix">
        <span className="product-discount-label">20% Off</span>
        <a className="product-remove-btn" href="#"></a>
        <div className="product-image"></div>
        <h2>Fruit Palm - Gift Ramadan</h2>
        <p>Golden brown with smooth, semi-translucent skin and flesh that is delicate
        and toffee flavoured, but not too sweet.</p>
        <span className="product-price">$70</span>
        <span className="product-discounted-price">$50</span>
        <button className="product-add-to-cart-btn">Add to Cart</button>
      </div>
    </div>
  );
};

export default Wishlist;
