import React from 'react';

const Feedback = () => {
  return (
    <div id="feedback" className="clearfix">
      <div id="feedback-description-container">
        <span id="rating-label-icon"></span>
        <h3>Rate the Following Questions</h3>
        <label>- Rate 1 start for very hard and 5 stars for very easy -</label>
      </div>
      <div id="feedback-first-column">
        <div className="question">
          <span>1. How would you rate you overall experience with Fodakty?</span>
          <div className="feedback-rating">
            <span className="rating-star-full"></span>
            <span className="rating-star-full"></span>
            <span className="rating-star-full"></span>
            <span className="rating-star-full"></span>
            <span className="rating-star-full"></span>
          </div>
        </div>
        <div className="question">
          <span>2. How easy was it finding products?</span>
          <div className="feedback-rating">
            <span className="rating-star"></span>
            <span className="rating-star"></span>
            <span className="rating-star"></span>
            <span className="rating-star"></span>
            <span className="rating-star"></span>
          </div>
        </div>
        <div className="question">
          <span>3. How easy was it making purchases?</span>
          <div className="feedback-rating">
            <span className="rating-star"></span>
            <span className="rating-star"></span>
            <span className="rating-star"></span>
            <span className="rating-star"></span>
            <span className="rating-star"></span>
          </div>
        </div>
      </div>
      <div id="feedback-second-column">
        <div className="question">
          <span>4. Was enough info (including images) provided for the products?</span>
          <div className="feedback-rating">
            <span className="rating-star"></span>
            <span className="rating-star"></span>
            <span className="rating-star"></span>
            <span className="rating-star"></span>
            <span className="rating-star"></span>
          </div>
        </div>
        <div className="question">
          <span>5. And finally, how would you rate the look and feel of the app?</span>
          <div className="feedback-rating">
            <span className="rating-star"></span>
            <span className="rating-star"></span>
            <span className="rating-star"></span>
            <span className="rating-star"></span>
            <span className="rating-star"></span>
          </div>
        </div>
        <button>Submit</button>
      </div>
    </div>
  );
};

export default Feedback;
