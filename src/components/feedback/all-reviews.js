import React from 'react';

const AllReviews = () => {
  return (
    <div id="all-reviews">
      <div className="review">
        <label>Username 1</label>
        <div id="rating" className="clearfix">
          <span className="rated">Rated</span>
          <span className="review-rating">5.0</span>
        </div>
        <div>
          <h2>Review Title</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis quis cursus justo.
          Proin condimentum fringilla leo non condimentum. Ut elit magna, aliquam quis tortor vel,
          porttitor iaculis odio.</p>
        </div>
      </div>
      <div className="review">
        <label>Username 1</label>
        <div id="rating" className="clearfix">
          <span className="rated">Rated</span>
          <span className="review-rating">5.0</span>
        </div>
        <div>
          <h2>Review Title</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis quis cursus justo.
          Proin condimentum fringilla leo non condimentum. Ut elit magna, aliquam quis tortor vel,
          porttitor iaculis odio.</p>
        </div>
      </div>
      <div className="review">
        <label>Username 1</label>
        <div id="rating" className="clearfix">
          <span className="rated">Rated</span>
          <span className="review-rating">5.0</span>
        </div>
        <div>
          <h2>Review Title</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis quis cursus justo.
          Proin condimentum fringilla leo non condimentum. Ut elit magna, aliquam quis tortor vel,
          porttitor iaculis odio.</p>
        </div>
      </div>
    </div>
  );
};

export default AllReviews;
