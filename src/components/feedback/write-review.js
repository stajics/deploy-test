import React from 'react';

const WriteReview = () => {
  return (
    <div id="write-review">
      <button className="close-btn">X</button>
      <div id="review-to-be-posted">
        <label id="write-review-lbl">Write a Review</label>
          <div id="write-review-content-area">
            <div id="write-review-rating">
              <label>Rate this Product</label>
                <div id="write-review-rating-stars">
                  <span className="rating-star"></span>
                  <span className="rating-star"></span>
                  <span className="rating-star"></span>
                  <span className="rating-star"></span>
                  <span className="rating-star"></span>
                </div>
            </div>
            <label>Review Title</label>
            <input type="text" />
            <label>Review (min 100 characters)</label>
            <textarea defaultValue="Please, write your review here."/>
            <button>Submit</button>
        </div>
    </div>
    </div>
  );
};

export default WriteReview;
