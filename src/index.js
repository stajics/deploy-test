import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import WebNavigation from './components/navigation/web-navigation';
import Header from './components/common/header';
import Home from './components/common/home';
import AboutUS from './components/common/about-us';
import Login from './components/user/login';
import Registration from './components/user/registration';
import ContactUS from './components/support/contact-us';
import Feedback from './components/feedback/feedback';
import Addresses from './components/user/addresses';
import NewAddress from './components/user/new-address';
import Inquiry from './components/support/inquiry';
import EditProfile from './components/user/edit-profile';
import WriteReview from './components/feedback/write-review';
import AllReviews from './components/feedback/all-reviews';
import Deals from './components/products/deals';
import Products from './components/products/products';
import ProductZoomed from './components/products/product-zoomed';
import Search from './components/products/search';
import SearchResult from './components/products/search-result';
import Wishlist from './components/products/wishlist';
import Coupon from './components/orders/coupon';
import Cart from './components/orders/cart';
import Orders from './components/orders/orders';
import Checkout from './components/orders/checkout';
import SuccessfulOrder from './components/orders/successful-order';
import ReturnOrder from './components/orders/return-order';
import ProductCustomizationBoxType from './components/product-customization/product-customization-box-type';
import ProductCustomizationOneNote from './components/product-customization/product-customization-one-note';
import ProductCustomizationColor from './components/product-customization/product-customization-color';
import ProductCustomizationDates from './components/product-customization/product-customization-dates';
import ProductCustomizationRibbon from './components/product-customization/product-customization-ribbon';
import SubmitEmailMessage from './components/product-customization/submit-email-message';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = null;
  };

  render() {
    return (
        <div>
          <WebNavigation />
          <Header />
          {/* <Home /> */}
          {/* <AboutUS /> */}
          <Login /> 
          {/* <Registration /> */}
          {/* <ContactUS /> */}
          {/* <Feedback /> */}
          {/* <WriteReview /> */}
          {/* <AllReviews /> */}
          {/* <Addresses /> */}
          {/* <NewAddress />  */}
          {/* <Inquiry /> */}
          {/* <EditProfile /> */}
          {/* <Deals /> */}
          {/* <Products /> */}
          {/* <ProductZoomed /> */}
          {/* <SubmitEmailMessage /> */}
          {/* <Search /> */}
          {/* <SearchResult /> */}
          {/* <Wishlist /> */}
          {/* <Coupon /> */}
          {/* <Cart /> */}
          {/* <Orders /> */}
          {/* <Checkout /> */}
          {/* <SuccessfulOrder /> */}
          {/* <ReturnOrder /> */}
          {/* <ProductCustomizationBoxType /> */}
          {/* <ProductCustomizationOneNote /> */}
          {/* <ProductCustomizationColor /> */}
          {/* ProductCustomizationDates /> */}
          {/* <ProductCustomizationRibbon /> */}
        </div>
    );
  }
}

ReactDOM.render(<App />, document.querySelector('.main-container'));
